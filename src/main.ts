import { Response, Request } from 'express'
import jwt from 'jsonwebtoken'
import express from 'express'
import { users } from './dummy-data/data.js'
import cors from 'cors'

const app = express()
const secret = 'secret-key'

interface Login {
  email: string
  password: string
}

app.use(express.json())

app.get('/', (req: Request, res: Response) => {
  res.status(200).send('Welcome in exo for Amine server !')
})
app.get('/users', (req: Request, res: Response) => {
  res.status(200).send({ users })
})

app.get('/users/:id', (req: Request, res: Response) => {
  const { id } = req.params

  const user = users.find(user => user.id === +id)

  if (user) {
    res.status(200).send(user)
  } else {
    res.status(404).send('User not found')
  }
})

app.post('/users', (req: Request, res: Response) => {
  if (!req.body) {
    throw new Error()
  }
  const data: Login = req.body

  const user = users.find(user => user.email === data.email)

  if (!user) {
    return res.status(401).send('Bad credentials !')
  }
  if (user?.password === data.password) {
    const token = jwt.sign(user, secret)
    return res.status(200).send({ token })
  } else {
    return res.status(401).send('Bad credentials !')
  }
})

app.use(cors())

app.listen(3000, () => {
  console.log('Server started on port 3000')
})
